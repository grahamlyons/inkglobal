Ink Global
==========

Berlin Clock Test
-----------------

The design puts the logic for parsing the time into the `BerlinClock` class and uses `ClockState`, the classes inheriting from `Row` and the `Lamp` enum to represent the state. The data classes also validate whatever is passed to them.

The data classes are immutable because once they have been instantiated there is no need for them to change their state. The children of `Row` are instantiated with factory methods to allow validation code to be placed in the parent class but use properties defined on the child (`LENGTH` in this case). The `Lamp` state is an enum because it only has state and there are no manipulations or validations on it.

Each of the individual classes has unit tests for their functionality and the `BerlinClock` class is covered by the acceptance tests as it represents the operation of the whole application. The acceptance criteria were written at the start to inform the design.
