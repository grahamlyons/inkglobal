package com.inkglobal.techtest;

public enum Lamp {

    YELLOW("Y"),
    RED("R"),
    OFF("O");

    private final String representation;

    private Lamp(String representation) {
        this.representation = representation;
    }

    @Override
    public String toString() {
        return this.representation;
    }

}
