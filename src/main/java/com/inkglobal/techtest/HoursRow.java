package com.inkglobal.techtest;

import java.util.Arrays;

public class HoursRow extends Row {

    public static final Integer LENGTH = 4;

    private HoursRow(Lamp[] lamps) {
        super(lamps);
    }

    public static HoursRow create(Lamp[] lamps) throws RowLengthException, RowContentException {
        Row.validateLength(lamps, LENGTH);
        Row.validateOrder(lamps);
        if(Arrays.asList(lamps).contains(Lamp.YELLOW)) {
            throw new RowContentException("Hours row cannot contain Yellow lamps");
        }
        return new HoursRow(lamps);
    }

}
