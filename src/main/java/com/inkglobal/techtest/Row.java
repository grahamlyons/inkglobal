package com.inkglobal.techtest;

public abstract class Row {

    private final Lamp[] lamps;

    protected Row(Lamp[] lamps) {
        this.lamps = lamps;
    }

    protected static boolean validateLength(Lamp[] lamps, Integer length) throws RowLengthException {
        if(lamps.length != length) {
            throw new RowLengthException(String.format("Must contain %1$d lamps", length));
        }
        return true;
    }

    protected static boolean validateOrder(Lamp[] lamps) throws RowContentException {
        Lamp lastState = null;
        for(Lamp lamp : lamps) {
            if(lastState == null) {
                lastState = lamp;
            } else if(lastState == Lamp.OFF && lamp != Lamp.OFF) {
                throw new RowContentException("Cannot have off lamps preceeding on lamps in sequence");
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder(this.lamps.length);
        for(Lamp lamp : this.lamps) {
            output.append(lamp);
        }
        return output.toString();
    }

}
