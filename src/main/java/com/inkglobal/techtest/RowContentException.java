package com.inkglobal.techtest;

public class RowContentException extends Exception {

    public RowContentException(String message) {
        super(message);
    }

}
