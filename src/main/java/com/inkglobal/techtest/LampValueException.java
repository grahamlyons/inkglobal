package com.inkglobal.techtest;

public class LampValueException extends Exception {

    public LampValueException(String message) {
        super(message);
    }

}
