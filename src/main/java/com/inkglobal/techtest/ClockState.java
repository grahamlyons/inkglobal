package com.inkglobal.techtest;

/**
 * Represents the state, for a given time, of the different rows
 * in the Berlin Clock.
 */
public class ClockState {

    private final Lamp topLamp;
    private final HoursRow topHours;
    private final HoursRow bottomHours;
    private final TopMinutesRow topMinutes;
    private final BottomMinutesRow bottomMinutes;

    public ClockState(Lamp topLamp, HoursRow topHours,
            HoursRow bottomHours, TopMinutesRow topMinutes,
            BottomMinutesRow bottomMinutes) throws LampValueException {
        if(topLamp == Lamp.RED) {
            throw new LampValueException("Top Lamp cannot be Red");
        }
        this.topLamp = topLamp;
        this.topHours = topHours;
        this.bottomHours = bottomHours;
        this.topMinutes = topMinutes;
        this.bottomMinutes =  bottomMinutes;
    }

    public Lamp getTopLamp() {
        return this.topLamp;
    }

    public HoursRow getTopHours() {
        return this.topHours;
    }

    public HoursRow getBottomHours() {
        return this.bottomHours;
    }

    public TopMinutesRow getTopMinutes() {
        return this.topMinutes;
    }

    public BottomMinutesRow getBottomMinutes() {
        return this.bottomMinutes;
    }

}
