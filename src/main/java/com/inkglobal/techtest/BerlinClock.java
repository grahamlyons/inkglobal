package com.inkglobal.techtest;

import java.util.Arrays;

public class BerlinClock {

    protected static final String TIME_DELIMITER = ":";

    /**
     * For a given time in the format "hh:mm:ss" returns a ClockState
     * object.
     */
    public ClockState getState(String time) throws LampValueException,
            RowLengthException, RowContentException {
        Integer[] parts = stringsToIntegers(time.split(TIME_DELIMITER));
        return new ClockState(
            getTopLamp(parts[2]),
            getTopHoursRow(parts[0]),
            getBottomHoursRow(parts[0]),
            getTopMinutesRow(parts[1]),
            getBottomMinutesRow(parts[1])
        );
    }

    protected Integer[] stringsToIntegers(String[] strings) {
        Integer[] integers = new Integer[strings.length];
        for(int i=0; i<strings.length; i++) {
            integers[i] = new Integer(strings[i]);
        }
        return integers;
    }

    protected Lamp[] buildLamps(Integer numberOn, Lamp onState) {
        Lamp[] lamps = new Lamp[4];
        for(int i=0; i<lamps.length; i++) {
            if(numberOn == 0) {
                lamps[i] = Lamp.OFF;
            } else {
                lamps[i] = onState;
                numberOn--;
            }
        }
        return lamps;
    }

    protected Lamp getTopLamp(Integer seconds) throws LampValueException,
            RowLengthException, RowContentException {
        Lamp state = Lamp.OFF;
        if(seconds % 2 == 0) {
            state = Lamp.YELLOW;
        }
        return state;
    }

    protected HoursRow getTopHoursRow(Integer hours) throws LampValueException,
            RowLengthException, RowContentException {
        Integer numberOn = hours / 5;
        Lamp[] lamps = buildLamps(numberOn, Lamp.RED);
        return HoursRow.create(lamps);
    }

    protected HoursRow getBottomHoursRow(Integer hours) throws LampValueException,
            RowLengthException, RowContentException {
        Integer numberOn = hours % 5;
        Lamp[] lamps = buildLamps(numberOn, Lamp.RED);
        return HoursRow.create(lamps);
    }

    protected TopMinutesRow getTopMinutesRow(Integer minutes) throws LampValueException,
            RowLengthException, RowContentException {
        Integer numberOn = minutes / 5;
        Lamp[] lamps = new Lamp[11];
        for(int i=0; i< lamps.length; i++) {
            if(numberOn == 0) {
                lamps[i] = Lamp.OFF;
            } else {
                if(Arrays.asList(2, 5, 8).contains(i)) {
                    lamps[i] = Lamp.RED;
                } else {
                    lamps[i] = Lamp.YELLOW;
                }
                numberOn--;
            }
        }
        return TopMinutesRow.create(lamps);
    }

    protected BottomMinutesRow getBottomMinutesRow(Integer minutes) throws LampValueException,
            RowLengthException, RowContentException {
        Integer numberOn = minutes % 5;
        Lamp[] lamps = buildLamps(numberOn, Lamp.YELLOW);
        return BottomMinutesRow.create(lamps);
    }

}
