package com.inkglobal.techtest;

import java.util.Arrays;

public class BottomMinutesRow extends Row {

    public static final Integer LENGTH = 4;

    private BottomMinutesRow(Lamp[] lamps) {
        super(lamps);
    }

    public static BottomMinutesRow create(Lamp[] lamps) throws RowLengthException, RowContentException {
        Row.validateLength(lamps, LENGTH);
        Row.validateOrder(lamps);
        if(Arrays.asList(lamps).contains(Lamp.RED)) {
            throw new RowContentException("Hours row cannot contain Red lamps");
        }
        return new BottomMinutesRow(lamps);
    }

}
