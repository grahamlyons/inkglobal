package com.inkglobal.techtest;

import java.util.Arrays;

public class TopMinutesRow extends Row {

    public static final Integer LENGTH = 11;

    private TopMinutesRow(Lamp[] lamps) {
        super(lamps);
    }

    public static TopMinutesRow create(Lamp[] lamps) throws RowLengthException, RowContentException {
        Row.validateLength(lamps, LENGTH);
        Row.validateOrder(lamps);
        for(int i=0; i<lamps.length; i++) {
            if(Arrays.asList(2, 5, 8).contains(i)) {
                if(lamps[i] == Lamp.YELLOW) {
                    throw new RowContentException(String.format("Cannot have a Yellow lamp in position %1$d", i+1));
                }
            } else if(lamps[i] == Lamp.RED) {
                throw new RowContentException(String.format("Cannot have a Red lamp in position %1$d", i+1));
            }
        }
        return new TopMinutesRow(lamps);
    }

}
