package com.inkglobal.techtest;

public class RowLengthException extends Exception {

    public RowLengthException(String message) {
        super(message);
    }

}
