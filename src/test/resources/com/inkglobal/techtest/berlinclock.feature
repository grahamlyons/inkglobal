Feature: Berlin Clock

    Scenario: Time Representation - midnight
        Given I have a Berlin Clock
        When I ask for a representation of the time "00:00:00"
        Then the top lamp should be yellow
        And the first hour row should be in state "OOOO"
        And the second hour row should be in state "OOOO"
        And the first minute row should be in state "OOOOOOOOOOO"
        And the second minute row should be in state "OOOO"

    Scenario: Time Representation - early afternoon
        Given I have a Berlin Clock
        When I ask for a representation of the time "13:17:01"
        Then the top lamp should be off
        And the first hour row should be in state "RROO"
        And the second hour row should be in state "RRRO"
        And the first minute row should be in state "YYROOOOOOOO"
        And the second minute row should be in state "YYOO"

    Scenario: Time Representation - just before midnight
        Given I have a Berlin Clock
        When I ask for a representation of the time "23:59:59"
        Then the top lamp should be off
        And the first hour row should be in state "RRRR"
        And the second hour row should be in state "RRRO"
        And the first minute row should be in state "YYRYYRYYRYY"
        And the second minute row should be in state "YYYY"

    Scenario: Time Representation - alternate midnight represenation
        Given I have a Berlin Clock
        When I ask for a representation of the time "24:00:00"
        Then the top lamp should be yellow
        And the first hour row should be in state "RRRR"
        And the second hour row should be in state "RRRR"
        And the first minute row should be in state "OOOOOOOOOOO"
        And the second minute row should be in state "OOOO"
