package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.PendingException;

public class StepDefinitions {

    private BerlinClock clock;
    private ClockState clockState;

    @Given("^I have a Berlin Clock$")
    public void i_have_a_Berlin_Clock() throws Throwable {
        clock = new BerlinClock();
    }

    @When("^I ask for a representation of the time \"(.*?)\"$")
    public void i_ask_for_a_representation_of_the_time(String time) throws Throwable {
        clockState = clock.getState(time);
        assertNotNull(clockState);
    }

    @Then("^the top lamp should be (yellow|off)$")
    public void the_top_lamp_should_be_(String state) throws Throwable {
        String topLamp = clockState.getTopLamp().toString();
        if(state.equals("yellow")) {
            assertEquals("Y", topLamp);
        } else {
            assertEquals("O", topLamp);
        }
    }

    @Then("^the first hour row should be in state \"(.*?)\"$")
    public void the_first_hour_row_should_be_in_state(String state) throws Throwable {
        String topHours = clockState.getTopHours().toString();
        assertEquals(state, topHours);
    }

    @Then("^the second hour row should be in state \"(.*?)\"$")
    public void the_second_hour_row_should_be_in_state(String state) throws Throwable {
        String bottomHours = clockState.getBottomHours().toString();
        assertEquals(state, bottomHours);
    }

    @Then("^the first minute row should be in state \"(.*?)\"$")
    public void the_first_minute_row_should_be_in_state(String state) throws Throwable {
        String topMinutes = clockState.getTopMinutes().toString();
        assertEquals(state, topMinutes);
    }

    @Then("^the second minute row should be in state \"(.*?)\"$")
    public void the_second_minute_row_should_be_in_state(String state) throws Throwable {
        String bottomMinutes = clockState.getBottomMinutes().toString();
        assertEquals(state, bottomMinutes);
    }

}
