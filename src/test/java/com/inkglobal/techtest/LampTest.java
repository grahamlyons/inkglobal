package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LampTest {

    @Test
    public void testYellowLampRepresentation() {
        Lamp lamp = Lamp.YELLOW;
        assertEquals("Y", lamp.toString());
    }

    @Test
    public void testRedLampRepresentation() {
        Lamp lamp = Lamp.RED;
        assertEquals("R", lamp.toString());
    }

    @Test
    public void testOffLampRepresentation() {
        Lamp lamp = Lamp.OFF;
        assertEquals("O", lamp.toString());
    }

}
