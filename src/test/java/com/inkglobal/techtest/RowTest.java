package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RowTest {

    @Test
    public void testCreateTopHours() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.RED, Lamp.OFF, Lamp.OFF, Lamp.OFF};
        Row hours = HoursRow.create(lamps);
        assertEquals("ROOO", hours.toString());
    }

    @Test
    public void testCreateTopMinutesOne() throws RowLengthException, RowContentException {
        Lamp[] lamps = {
            Lamp.YELLOW, Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF
        };
        Row topMinutes = TopMinutesRow.create(lamps);
        assertEquals("YOOOOOOOOOO", topMinutes.toString());
    }

    @Test
    public void testCreateTopMinutesTwo() throws RowLengthException, RowContentException {
        Lamp[] lamps = {
            Lamp.YELLOW, Lamp.YELLOW, Lamp.RED, Lamp.YELLOW,
            Lamp.YELLOW, Lamp.RED, Lamp.YELLOW, Lamp.YELLOW,
            Lamp.RED, Lamp.YELLOW, Lamp.YELLOW
        };
        Row topMinutes = TopMinutesRow.create(lamps);
        assertEquals("YYRYYRYYRYY", topMinutes.toString());
    }

    @Test
    public void testCreateBottomMinutes() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.YELLOW, Lamp.OFF, Lamp.OFF, Lamp.OFF};
        Row bottomMinutes = BottomMinutesRow.create(lamps);
        assertEquals("YOOO", bottomMinutes.toString());
    }

    @Test(expected=RowLengthException.class)
    public void testInvalidHoursLampLength() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.OFF};
        Row hours = HoursRow.create(lamps);
    }

    @Test(expected=RowLengthException.class)
    public void testInvalidTopMinutesLampLength() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.OFF};
        Row hours = TopMinutesRow.create(lamps);
    }

    @Test(expected=RowLengthException.class)
    public void testInvalidBottomMinutesLampLength() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.OFF};
        Row hours = BottomMinutesRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidHoursYellowContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.YELLOW, Lamp.YELLOW, Lamp.YELLOW, Lamp.YELLOW};
        Row hours = HoursRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidHoursOrderContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.OFF, Lamp.YELLOW, Lamp.YELLOW, Lamp.YELLOW};
        Row hours = HoursRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidBottomMinutesRedContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.RED, Lamp.RED, Lamp.RED, Lamp.RED};
        Row hours = BottomMinutesRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidTopMinutesOrderContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {
            Lamp.OFF, Lamp.YELLOW, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF
        };
        Row topMinutes = TopMinutesRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidBottomMinutesOrderContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {Lamp.OFF, Lamp.YELLOW, Lamp.OFF, Lamp.OFF};
        Row bottomMinutes = BottomMinutesRow.create(lamps);
    }

    @Test(expected=RowContentException.class)
    public void testInvalidTopMinutesColourContent() throws RowLengthException, RowContentException {
        Lamp[] lamps = {
            Lamp.YELLOW, Lamp.YELLOW, Lamp.YELLOW, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF
        };
        Row topMinutes = TopMinutesRow.create(lamps);
    }

}
