package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClockStateTest {

    public ClockStateTest() throws RowLengthException, RowContentException {

    }

    private HoursRow topHours = HoursRow.create(
        new Lamp[]{
            Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF
        });
    private HoursRow bottomHours = HoursRow.create(
        new Lamp[]{
            Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF
        });
    private TopMinutesRow topMinutes = TopMinutesRow.create(
        new Lamp[]{
            Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF
        });
    private BottomMinutesRow bottomMinutes = BottomMinutesRow.create(
        new Lamp[]{
            Lamp.OFF, Lamp.OFF,
            Lamp.OFF, Lamp.OFF
        });

    @Test
    public void testGetTopLamp() throws LampValueException,
            RowLengthException, RowContentException {
        Lamp topLamp = Lamp.YELLOW;
        ClockState state = new ClockState(
            topLamp,
            topHours,
            bottomHours,
            topMinutes,
            bottomMinutes
        );
        assertEquals(topLamp, state.getTopLamp());
    }

    @Test(expected=LampValueException.class)
    public void testInvalidLampValue() throws LampValueException,
            RowLengthException, RowContentException {
        ClockState state = new ClockState(
            Lamp.RED,
            topHours,
            bottomHours,
            topMinutes,
            bottomMinutes
        );
    }

}
